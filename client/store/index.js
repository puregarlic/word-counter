import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const state = {
  count: 0
};

const mutations = {
  COUNT (state, wordCount) {
    console.log(wordCount);
    state.count = wordCount;
  }
};

const actions = {
};

const getters = {
  count: (state) => {
    return state.count;
  }
};

const store = new Vuex.Store({
  state,
  mutations,
  actions,
  getters
});

export default store;
